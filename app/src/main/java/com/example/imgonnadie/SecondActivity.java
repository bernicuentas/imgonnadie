package com.example.imgonnadie;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Random;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private int randomNum, age, km, causas = 0;
    private Button bSend;
    private EditText edName, edPlace, edBirthday;
    private Spinner spDiet, spSex, spStrength;
    private String sName, sPlace, sBirthday, sDiet, sSex, sStrength, causa,result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        bSend = findViewById(R.id.send);
        bSend.setOnClickListener(this);
    }

    public void calculateYourDead(){
        //edad de defuncion
        Random random = new Random();
        age = 2020 - Integer.parseInt(sBirthday);
        randomNum = random.nextInt(50) + age;
        km = random.nextInt(1000);

        switch (sDiet){
            case "Real food":
                causas++;
                break;
            case "Mediterranean":
                causas += 2;
                break;
            case "Without diet":
                causas += 4;
                break;
        }

        switch (sStrength){
            case "Athlete":
                causas++;
                break;
            case "Peaceful":
                causas += 2;
                break;
            case "Pleasure seeking":
                causas += 4;
                break;
        }

        if (causas <= 2){
            causa = " of natural death ";
        } else if (causas <= 4){
            causa = " of traffic accident ";
        } else if (causas <= 6){
            causa = " of bank robbery ";
        } else if (causas <= 8){
            causa = " of obesity ";
        }

        result = sName.substring(0, 1).toUpperCase() + sName.substring(1)
                + " will die as a result" + causa + "at the age of " + randomNum
                + " about " + km + " km from his hometown, " +
                sPlace.substring(0, 1).toUpperCase() + sPlace.substring(1) + ".";
    }


    public void getVar(){
        edName = findViewById(R.id.editTextName);
        edPlace = findViewById(R.id.editTextPlace);
        edBirthday = findViewById(R.id.editTextBirthday);

        sName = edName.getText().toString();
        sPlace = edPlace.getText().toString();
        sBirthday = edBirthday.getText().toString();

        spDiet = findViewById(R.id.spinnerDiet);
        spSex = findViewById(R.id.spinnerSex);
        spStrength = findViewById(R.id.spinnerStrength);

        sDiet = spDiet.getSelectedItem().toString();
        sSex = spSex.getSelectedItem().toString();
        sStrength = spStrength.getSelectedItem().toString();
    }

    @Override
    public void onClick(View v) {
        getVar();
        calculateYourDead();
        Intent i = new Intent(this, ThirdActivity.class);
        i.putExtra("result", result);
        startActivity(i);

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        getVar();
        outState.putString("sName", sName);
        outState.putString("sPlace", sPlace);
        outState.putString("sBirthday", sBirthday);
        outState.putString("sDiet", sDiet);
        outState.putString("sSex", sSex);
        outState.putString("sStrength", sStrength);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.sName = savedInstanceState.getString("sName");
        this.sPlace = savedInstanceState.getString("sPlace");
        this.sBirthday = savedInstanceState.getString("sBirthday");
        this.sDiet = savedInstanceState.getString("sDiet");
        this.sSex = savedInstanceState.getString("sSex");
        this.sStrength = savedInstanceState.getString("sStrength");

    }
}
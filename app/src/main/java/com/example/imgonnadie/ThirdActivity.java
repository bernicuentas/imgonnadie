package com.example.imgonnadie;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {

    private TextView textView;
    ConstraintLayout thirdLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        textView = findViewById(R.id.textView);
        String result = getIntent().getStringExtra("result");
        textView.setText(result);

        thirdLayout = findViewById(R.id.thirdLayout);

        Drawable third = getResources().getDrawable(R.drawable.bg2);
        thirdLayout.setBackground(third);
    }
}
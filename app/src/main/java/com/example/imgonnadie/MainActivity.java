package com.example.imgonnadie;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mainButton;
    ConstraintLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainButton = findViewById(R.id.mainButton);
        mainButton.setOnClickListener(this);
        mainLayout = findViewById(R.id.mainLayout);

        Drawable main = getResources().getDrawable(R.drawable.bg1);
        mainLayout.setBackground(main);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mainButton)
        {
            Intent i = new Intent(this, SecondActivity.class);

            startActivity(i);
        }
    }


}